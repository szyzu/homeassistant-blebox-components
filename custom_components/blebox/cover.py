"""
Support for Blebox Shutterbox covers.

"""
import logging

import voluptuous as vol
import requests

from homeassistant.components.cover import (CoverDevice, PLATFORM_SCHEMA)
from homeassistant.const import (
    CONF_HOST, CONF_FRIENDLY_NAME,CONF_COVERS)
import homeassistant.helpers.config_validation as cv

_LOGGER = logging.getLogger(__name__)

COVER_SCHEMA = vol.Schema({
    vol.Optional(CONF_HOST, default='true'): cv.string,
    vol.Optional(CONF_FRIENDLY_NAME): cv.string,
})

PLATFORM_SCHEMA = PLATFORM_SCHEMA.extend({
    vol.Required(CONF_COVERS): vol.Schema({cv.slug: COVER_SCHEMA}),
})


def setup_platform(hass, config, add_devices, discovery_info=None):
    """Set up cover controlled by Blebox."""
    devices = config.get(CONF_COVERS, {})
    covers = []

    for device_name, device_config in devices.items():
        try:
            covers.append(
                BleboxCover(
                    hass,
                    device_config.get(CONF_FRIENDLY_NAME, device_name),
                    device_config.get(CONF_HOST),
                )
            )
        except Exception as e:
            _LOGGER.error(e)

    if not covers:
        _LOGGER.error("No covers added")
        return False

    add_devices(covers)

class BleboxCover(CoverDevice):
    """Representation a blebox cover."""
    
    @staticmethod
    def _find_value(val, dic):
        for key,value in dic.items():
            if key == val[0]:
                val.pop(0)
                if len(val)==0:
                    return value
                return BleboxCover._find_value(val[:],value)                
            if type(value)==dict:
                subDicVal = BleboxCover._find_value(val[:],value)
                if subDicVal:
                    return subDicVal
        return None
    

    def __init__(self, hass, name, host):
        self._hass = hass
        self._name = name
        r = requests.get('http://%s/api/device/state' % host)
        if r.status_code==200:
            json = r.json()
            if BleboxCover._find_value(['type'],json)=='shutterBox':
                if self._name is None:
                    self._name = BleboxCover._find_value(['deviceName'],json)
                self._url_open = 'http://%s/s/u' % host 
                self._url_close = 'http://%s/s/d' % host
                self._url_stop = 'http://%s/s/s' % host
                self._url_get_state = 'http://%s/api/shutter/state' % host
                if ((int)(BleboxCover._find_value(['apiLevel'],json)))>=20180604:
                    self._url_get_state = 'http://%s/api/shutter/extended/state' % host
                self._url_set_state = 'http://%s/s/p/' % host 
                self._state = None
                self._is_calibrated = False
                r = requests.get(self._url_get_state)
                if r.status_code==200:
                    self._is_calibrated = BleboxCover._find_value(['calibrationParameters','isCalibrated'],r.json())==1
        else:
            raise Exception('Device unavailable')
    
    @staticmethod
    def _send_request(url):
        if url is not None:
            _LOGGER.info("Requesting GET: %s", url)
            r=requests.get(url)
            success=r.status_code==200
            if success:
                return r.json()
            else:
                _LOGGER.error("Requesting GET: %s", url)
        return None

    @staticmethod
    def _position_template(value):
        if value in range(0,101):
            return 100-value
        return None
        
    def _move_cover_and_update(self, url):
        response = self._send_request(url)
        if response is not None:
            position = (int)(self._find_value(['currentPos','position'],response))
            self._state = self._position_template(position)
        else:
            self._state = None
        return None

    @property
    def should_poll(self):
        return self._is_calibrated

    @property
    def name(self):
        return self._name

    @property
    def is_closed(self):
        if self.current_cover_position is not None:
            return self.current_cover_position == 0

    @property
    def current_cover_position(self):
        """Return current position of cover.

        None is unknown, 0 is closed, 100 is fully open.
        """
        return self._state

    def update(self):
        """Update device state."""
        self._move_cover_and_update(self._url_get_state)

    def open_cover(self, **kwargs):
        """Open the cover."""
        self._move_cover_and_update(self._url_open)

    def close_cover(self, **kwargs):
        """Close the cover."""
        self._move_cover_and_update(self._url_close)

    def stop_cover(self, **kwargs):
        """Stop the cover."""
        self._move_cover_and_update(self._url_stop)

    def set_cover_position(self,position, **kwargs):
        """Move to specific position all or specified cover."""
        blebox_position = self._position_template(position)
        url = self._url_set_state+str(blebox_position)
        self._move_cover_and_update(url)

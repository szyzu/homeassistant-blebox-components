"""
Support for Blebox SwitchBox and SwitchBoxD.

"""
import logging

import voluptuous as vol
import requests

from homeassistant.components.switch import (SwitchDevice, PLATFORM_SCHEMA)
from homeassistant.const import (
    CONF_HOST, CONF_FRIENDLY_NAME, CONF_SWITCHES)
import homeassistant.helpers.config_validation as cv

_LOGGER = logging.getLogger(__name__)

SWITCH_SCHEMA = vol.Schema({
    vol.Optional(CONF_HOST, default='true'): cv.string,
    vol.Optional(CONF_FRIENDLY_NAME): cv.string,
})

PLATFORM_SCHEMA = PLATFORM_SCHEMA.extend({
    vol.Required(CONF_SWITCHES): vol.Schema({cv.slug: SWITCH_SCHEMA}),
})

def setup_platform(hass, config, add_devices, discovery_info=None):
    """Set up cover controlled by Blebox."""
    devices = config.get(CONF_SWITCHES, {})
    switches = []

    for device_name, device_config in devices.items():
        r = requests.get('http://'+device_config.get(CONF_HOST)+'/api/device/state')
        if r.status_code==200:
            _type = r.json()['device']['type']
            if _type == 'switchBox':                
                switches.append(
                    BleboxSwitch(
                        hass,
                        device_config.get(CONF_FRIENDLY_NAME, device_name),
                        device_config.get(CONF_HOST),
                        None
                    )
                )
            elif _type == 'switchBoxD':
                r = requests.get('http://'+device_config.get(CONF_HOST)+'/api/relay/state')
                if r.status_code==200 and r.json()['relays'] is not None:
                    for relay in r.json()['relays']:
                        switches.append(
                            BleboxSwitch(
                                hass,
                                device_config.get(CONF_FRIENDLY_NAME, device_name),
                                device_config.get(CONF_HOST),
                                relay['relay']
                            )
                        )

    if not switches:
        _LOGGER.error("No switches added")
        return False

    add_devices(switches)


class BleboxSwitch(SwitchDevice):
    """Representation a blebox cover."""

    def __init__(self, hass, name, host, relay_number):
        self._hass = hass
        self._name = name       
        if relay_number is not None:
            self._name = self._name + '_' + relay_number
        self._relay_number=relay_number  
        if relay_number is None:
            self._url_on = 'http://%s/s/1' % host 
            self._url_off = 'http://%s/s/0' % host
        else:
            self._url_on = 'http://%s/s/%d/1' % (host,relay_number) 
            self._url_off = 'http://%s/s/%d/0' % (host,relay_number)
        self._url_state = 'http://%s/api/relay/state' % host
        self._state = None
   
    @staticmethod
    def _send_request(url):
        if url is not None:
            _LOGGER.info("Requesting GET: %s", url)
            r=requests.get(url)
            success=r.status_code==200
            if success:
                return r.json()
            else:
                _LOGGER.error("Requesting GET: %s", url)
        return None

    def _change_relay_state_and_update(self, url):
        response = self._send_request(url)
        if response is not None:
            if self._relay_number is not None:
                for relay in response['relays']:
                    if relay['relay']==self._relay_number:
                        self._state = int(relay['state'])
            else:
                self._state = int(response[0]['state'])
        else:
            self._state = None
        return None

    @property
    def should_poll(self):
        return True

    @property
    def name(self):
        return self._name

    @property
    def is_on(self):
        if self._state is not None:
            return self._state == 1

    @property
    def assumed_state(self):
        return True

    def update(self):
        """Update device state."""
        self._change_relay_state_and_update(self._url_state)

    def turn_on(self, **kwargs):
        self._change_relay_state_and_update(self._url_on)

    def turn_off(self, **kwargs):
        self._change_relay_state_and_update(self._url_off)
